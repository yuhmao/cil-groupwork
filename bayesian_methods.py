import argparse
import pickle
from typing import Dict, List, Union

import numpy as np
import pandas as pd
from scipy import sparse as sps
import os

import myfm
from myfm import RelationBlock
from myfm.gibbs import MyFMGibbsRegressor, MyFMOrderedProbit
from myfm.utils.callbacks import (
    LibFMLikeCallbackBase,
    RegressionCallback,
)
from myfm.utils.encoders import CategoryValueToSparseEncoder
from util import read_data_in_dense_matrix


def impute_by_Bayesian(df_train, df_test, pred_data, use_iu=True, use_ii=False, ITERATION=10, DIMENSION=10, stricter_protocol=True):
    '''
        use_iu: use implicit user feature
        use_ii: use implicit item feature
        ITERATION: mcmc iteration
        DIMENSION: fm embedding dimension
        stricter_protocol: Whether to use the \"stricter\" protocol (i.e., don't include the test set implicit information) stated in [Rendle, '19].
    '''

    # data_manager = MovieLens100kDataManager()
    # df_train, df_test = get_data()
    ALGORITHM = 'regression'

    if stricter_protocol:
        implicit_data_source = df_train
    else:
        implicit_data_source = pd.concat([df_train, df_test])

    user_to_internal = CategoryValueToSparseEncoder[int](
        implicit_data_source.user_id.values
    )
    movie_to_internal = CategoryValueToSparseEncoder[int](
        implicit_data_source.movie_id.values
    )

    print(
        "df_train.shape = {}, df_test.shape = {}".format(df_train.shape, df_test.shape)
    )

    movie_vs_watched: Dict[int, List[int]] = dict()
    user_vs_watched: Dict[int, List[int]] = dict()

    for row in implicit_data_source.itertuples():
        user_id: int = row.user_id
        movie_id: int = row.movie_id
        movie_vs_watched.setdefault(movie_id, list()).append(user_id)
        user_vs_watched.setdefault(user_id, list()).append(movie_id)

    X_date_train, X_date_test = (None, None)

    # setup grouping
    feature_group_sizes = []

    feature_group_sizes.append(len(user_to_internal))  # user ids

    if use_iu:
        # all movies which a user watched
        feature_group_sizes.append(len(movie_to_internal))

    feature_group_sizes.append(len(movie_to_internal))  # movie ids

    if use_ii:
        feature_group_sizes.append(
            len(user_to_internal)  # all the users who watched a movies
        )

    grouping = [i for i, size in enumerate(feature_group_sizes) for _ in range(size)]

    # given user/movie ids, add additional infos and return it as sparse
    def augment_user_id(user_ids: List[int]) -> sps.csr_matrix:
        X = user_to_internal.to_sparse(user_ids)
        if not use_iu:
            return X
        data: List[float] = []
        row: List[int] = []
        col: List[int] = []
        for index, user_id in enumerate(user_ids):
            watched_movies = user_vs_watched.get(user_id, [])
            normalizer = 1 / max(len(watched_movies), 1) ** 0.5
            for mid in watched_movies:
                data.append(normalizer)
                col.append(movie_to_internal[mid])
                row.append(index)
        return sps.hstack(
            [
                X,
                sps.csr_matrix(
                    (data, (row, col)),
                    shape=(len(user_ids), len(movie_to_internal)),
                ),
            ],
            format="csr",
        )

    def augment_movie_id(movie_ids: List[int]) -> sps.csr_matrix:
        X = movie_to_internal.to_sparse(movie_ids)
        if not use_ii:
            return X

        data: List[float] = []
        row: List[int] = []
        col: List[int] = []

        for index, movie_id in enumerate(movie_ids):
            watched_users = movie_vs_watched.get(movie_id, [])
            normalizer = 1 / max(len(watched_users), 1) ** 0.5
            for uid in watched_users:
                data.append(normalizer)
                row.append(index)
                col.append(user_to_internal[uid])
        return sps.hstack(
            [
                X,
                sps.csr_matrix(
                    (data, (row, col)),
                    shape=(len(movie_ids), len(user_to_internal)),
                ),
            ]
        )

    # Create RelationBlock.
    train_blocks: List[RelationBlock] = []
    test_blocks: List[RelationBlock] = []
    for source, target in [(df_train, train_blocks), (df_test, test_blocks)]:
        unique_users, user_map = np.unique(source.user_id, return_inverse=True)
        target.append(RelationBlock(user_map, augment_user_id(unique_users)))
        unique_movies, movie_map = np.unique(source.movie_id, return_inverse=True)
        target.append(RelationBlock(movie_map, augment_movie_id(unique_movies)))

    trace_path = "rmse_{0}.csv".format(ALGORITHM)

    callback: LibFMLikeCallbackBase
    fm: Union[MyFMGibbsRegressor, MyFMOrderedProbit]

    fm = myfm.MyFMRegressor(rank=DIMENSION)
    callback = RegressionCallback(
        n_iter=ITERATION,
        X_test=X_date_test,
        y_test=df_test.rating.values,
        X_rel_test=test_blocks,
        clip_min=1,
        clip_max=5,
        trace_path=trace_path,
    )


    fm.fit(
        X_date_train,
        df_train.rating.values,
        X_rel=train_blocks,
        grouping=grouping,
        n_iter=ITERATION,
        n_kept_samples=ITERATION,
        callback=callback,
    )
    with open(
        "callback_result_{0}.pkl".format(ALGORITHM), "wb"
    ) as ofs:
        pickle.dump(callback, ofs)
    
    def _create_relational_blocks(df):
        blocks = []
        for source, target in [(df, blocks)]:
            unique_users, user_map = np.unique(source.user_id, return_inverse=True)
            target.append(RelationBlock(user_map, augment_user_id(unique_users)))
            unique_movies, movie_map = np.unique(source.movie_id, return_inverse=True)
            target.append(RelationBlock(movie_map, augment_movie_id(unique_movies)))
        return blocks

    # import pdb;pdb.set_trace()
    # pred_data = get_test()
    test_blocks = _create_relational_blocks(pred_data)
    predictions = fm.predict(None, test_blocks)
    
    # data_matrix, is_provided = read_data_in_dense_matrix('../../data/data_train.csv', n_col=1000, n_row=10000)
    pred_matrix = np.zeros((10000, 1000))

    row_id = pred_data['user_id'].to_numpy() 
    col_id = pred_data['movie_id'].to_numpy()
    pred_matrix[row_id, col_id] = predictions

    return pred_matrix
    # rmse = np.sqrt(np.mean((pred_matrix[row_id, col_id] - predictions) ** 2))
    # print(rmse)
    # raise

    # store_dense_matrix_to_submission(os.path.join('../data', 'sampleSubmission.csv'), os.path.join('../data', 'BayesianSVD++.csv'), pred_matrix)