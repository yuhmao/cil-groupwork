import numpy as np
import torch
import math
from util import move_param_to_device

def impute_by_mean(data_matrix, is_provided, mean_type, enforce_train=True):
    '''
    Return a clone of the data_matrix by replacing all unknown values by the mean value.
    mean_type can be 'row' or 'col'.
    !! This assumes missing values to be initialized as zero.
    '''
    n_row, n_col = data_matrix.shape
    if mean_type == 'row':
        mean_value = data_matrix.sum(axis=1, keepdims=True) / is_provided.sum(axis=1, keepdims=True)
        mean_matrix = np.tile(mean_value, (1, n_col))
    elif mean_type == 'col':
        mean_value = data_matrix.sum(axis=0, keepdims=True) / is_provided.sum(axis=0, keepdims=True)
        mean_matrix = np.tile(mean_value, (n_row, 1))
    if enforce_train:
        mean_matrix[is_provided] = data_matrix[is_provided]
    return mean_matrix

def _svd_approximation(data, keep_thre:float=None, keep_rank:int=None):
    assert keep_thre is not None or keep_rank is not None, "Must specify either threshold or rank for SVD approximation."
    assert keep_thre is None or keep_rank is None, "Cannot both specify threshold and rank for SVD approximation."

    # perform SVD approx, sigma will automatically be sorted from largest to smallest
    U, Sigma, VT = np.linalg.svd(data, full_matrices=False) 
    num_nonzero_sv = len(Sigma)
    # determine cut threshold
    if keep_thre:
        Sig2 = Sigma**2
        cumsum = np.cumsum(Sig2)
        thre = keep_thre * cumsum[-1]
        num_chosen_sv = np.sum(cumsum < thre) + 1 # keep enough sv to achieve approx error > thre
    elif keep_rank:
        num_chosen_sv = keep_rank

    if num_chosen_sv < num_nonzero_sv:
        data = (U[:, :num_chosen_sv] @ np.diag(Sigma[:num_chosen_sv]) @ VT[:num_chosen_sv, :])

    return data

def impute_by_svd(data_matrix, is_provided, iters:int, svd_keep_thre=0.9, start_from=None):
    '''
    Return a clone of the data_matrix by replacing all unknown values by SVD approximation with given threshold.
    Will start from a column mean imputation by default.
    '''
    if start_from is None:
        data = impute_by_mean(data_matrix, is_provided, mean_type='col')
    else:
        data = np.copy(start_from)

    for _ in range(iters):
        # ensure ground truth for train data
        data[is_provided] = data_matrix[is_provided]
        data = _svd_approximation(data, keep_thre=svd_keep_thre)

    return data

def impute_by_ALS_GD(data_matrix, is_provided, num_epoch:int, iters_per_epoch:int, rank:int, reg_weight:float):
    '''
    Implements alternative least square; steps are computed via gradient decent instead of inverse.
    '''
    # intialize
    nrow, ncol = data_matrix.shape
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    print("Using:", device)

    U = torch.rand(nrow, rank, device=device) / math.sqrt(rank)
    V = torch.rand(rank, ncol, device=device) / math.sqrt(rank)
    
    data_matrix, is_provided = torch.from_numpy(data_matrix).to(device), torch.from_numpy(is_provided).to(device)

    U_optimizer = torch.optim.Adam([U, ], lr=1e-1)
    V_optimizer = torch.optim.Adam([V, ], lr=1e-1)
    loss_monitor = []
    for epoch in range(num_epoch):
        # update U
        U.requires_grad, V.requires_grad = True, False
        for iter in range(iters_per_epoch):
            U_optimizer.zero_grad()
            loss = 0.5 * torch.norm((data_matrix - torch.matmul(U, V))[is_provided], p='fro') + 0.5 * reg_weight * (torch.norm(U, p='fro') + torch.norm(V, p='fro'))
            loss.backward()
            U_optimizer.step()

        # update V
        U.requires_grad, V.requires_grad = False, True
        for iter in range(iters_per_epoch):
            V_optimizer.zero_grad()
            loss = 0.5 * torch.norm((data_matrix - torch.matmul(U, V))[is_provided], p='fro') + 0.5 * reg_weight * (torch.norm(U, p='fro') + torch.norm(V, p='fro'))
            loss.backward()
            V_optimizer.step()

        with torch.no_grad():
            loss = 0.5 * torch.norm((data_matrix - torch.matmul(U, V))[is_provided], p='fro') + 0.5 * reg_weight * (torch.norm(U, p='fro') + torch.norm(V, p='fro'))
            loss_monitor.append(loss.item())
    
    return torch.matmul(U, V).cpu().detach().numpy(), loss_monitor


def impute_by_SVP(data_matrix, is_provided, iters:int, rank:int, step_size:float=0.1, start_from=None):
    '''
    Implements the singular value projection algorithm. In each iteration, we update by gradient descent w.r.t. the squared frobenius loss and then do SVD to project into low rank matrices.
    '''
    if start_from is None:
        approx = impute_by_mean(data_matrix, is_provided, mean_type='col', enforce_train=False)
    else:
        assert start_from.shape == data_matrix.shape, "Initialization should be at the consistent shape."
        approx = np.copy(start_from)
    for _ in range(iters):
        approx[is_provided] = approx[is_provided] + step_size * (data_matrix - approx)[is_provided]
        approx = _svd_approximation(approx, keep_rank=rank)
    approx = approx
    return approx

def _shrink_nuclear(data, shrink_thre:float):
    U, Sigma, VT = np.linalg.svd(data, full_matrices=False) 
    Sigma = (Sigma - shrink_thre).clip(min=0)
    num_chosen_sv = np.count_nonzero(Sigma)
    data = U[:, :num_chosen_sv] @ np.diag(Sigma[:num_chosen_sv]) @ VT[:num_chosen_sv, :]
    return data

def impute_by_nuclear_relaxation(data_matrix, is_provided, iters:int, nuclear_reg:float, step_size:float=0.1, start_from=None):
    '''
    Implements the nuclear norm relaxation algorithm.
    '''
    if start_from is None:
        approx = impute_by_mean(data_matrix, is_provided, mean_type='col', enforce_train=False)
    else:
        assert start_from.shape == data_matrix.shape, "Initialization should be at the consistent shape."
        approx = np.copy(start_from)

    for _ in range(iters):
        approx[is_provided] = approx[is_provided] + step_size * (data_matrix[is_provided] - approx[is_provided])
        approx = _shrink_nuclear(approx, nuclear_reg)
        
    return approx



def compute_shrunk_similarity(data_matrix, is_provided, shrunk=100):
    data_matrix = np.copy(data_matrix)
    data_matrix[~is_provided] = 0
    data_matrix = data_matrix - data_matrix.sum(axis=0, keepdims=True) / is_provided.sum(axis=0, keepdims=True) # extract column mean
    data_matrix[~is_provided] = 0
    data_matrix = data_matrix / np.sqrt((data_matrix**2).sum(axis=0, keepdims=True) / is_provided.sum(axis=0, keepdims=True)) # normalize by column std

    num_common_rates_penaltied = is_provided.astype(float).T @ is_provided.astype(float) + shrunk
    np.seterr(invalid='ignore') # suppress 0 / 0 warning
    corr = (data_matrix.T @ data_matrix) / num_common_rates_penaltied # compute correlation based on common raters
    np.seterr(invalid=None) # recover numpy behavior

    return corr

def compute_topk_similar_items(data_matrix, is_provided, k:int, shrunk=100):
    shrunk_corr = compute_shrunk_similarity(data_matrix, is_provided, shrunk)
    np.fill_diagonal(shrunk_corr, 0) # exclude itself
    shrunk_corr = np.abs(shrunk_corr)
    shrunk_corr[np.isnan(shrunk_corr)] = -1 # remove nan
    sim_order = np.argsort(shrunk_corr, axis=1)
    topk_sim = sim_order[:, -k:]
    return topk_sim
    
def impute_by_Koren_three_facet(data_matrix, is_provided, dim_embed:int, iters:int, first_reg:float, second_reg:float, third_reg:float=0, k:int=0, use_SVDpp=False):
    '''
    Reference: https://people.engr.tamu.edu/huangrh/Spring16/papers_course/matrix_factorization.pdf
    Three-Faceted model: 
    1. general facet about global, user and item
    2. iteraction between user and item
    3. neighborhood info about item
    '''

    num_user, num_item = data_matrix.shape
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    print("Using:", device)

    if not use_SVDpp:
        assert third_reg!=0 and k!=0, "Third tier model needs reg and k for params."
        topk_neighbors = torch.from_numpy(compute_topk_similar_items(data_matrix, is_provided, k))
        is_neighbor = torch.zeros(num_item, num_item).scatter_(1, topk_neighbors, 1)
        is_neighbor = is_neighbor.to(device)

    data_matrix, is_provided = torch.from_numpy(data_matrix).to(device), torch.from_numpy(is_provided).to(device)

    # Define the params for three tier model
    global_offset = torch.rand(1)
    user_offset = torch.rand(num_user, 1)
    item_offset = torch.rand(1, num_item)

    item_embedding_explicit = torch.rand(num_item, dim_embed)
    item_embedding_implicit = torch.rand(num_item, dim_embed)
    user_embedding = torch.rand(num_user, dim_embed)

    item_corr_weight = torch.rand(num_item, num_item)
    item_corr_weight.fill_diagonal_(0) # disallow correlation to itself to prevent data leakage
    item_corr_offset = torch.rand(num_item, num_item)

    param_list = [global_offset, user_offset, item_offset, item_embedding_explicit, item_embedding_implicit, user_embedding, item_corr_weight, item_corr_offset]
    move_param_to_device(param_list, device)
    optimizer = torch.optim.Adam(param_list, lr=5e-2)
    scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=[iters//8 * i for i in range(3,7)], gamma=0.2)
    factor_normalizer = torch.sqrt(is_provided.sum(dim=1, keepdim=True)).to(device)

    def build_integrated_model(param_list, factor_normalizer):
        global_offset, user_offset, item_offset, item_embedding_explicit, item_embedding_implicit, user_embedding, item_corr_weight, item_corr_offset = param_list
        first_model = global_offset + user_offset + item_offset
        user_embedding_with_item_preference = user_embedding + (is_provided.float() @ item_embedding_implicit) / factor_normalizer
        second_model = user_embedding_with_item_preference @ item_embedding_explicit.T

        integrated_model = first_model + second_model
        if not use_SVDpp:
            weighted_bias_from_current = (data_matrix - first_model) @ (item_corr_weight * is_neighbor)
            third_model = (weighted_bias_from_current + is_provided.float() @ is_neighbor.T @ item_corr_offset) / k**0.5
            integrated_model += third_model
        return integrated_model

    def build_regularization(first_reg, second_reg, third_reg, param_list):
        global_offset, user_offset, item_offset, item_embedding_explicit, item_embedding_implicit, user_embedding, item_corr_weight, item_corr_offset = param_list
        first_reg_loss = first_reg * (torch.norm(global_offset) + torch.norm(user_offset) + torch.norm(item_offset))
        second_reg_loss = second_reg * (torch.norm(item_embedding_explicit) + torch.norm(item_embedding_implicit) + torch.norm(user_embedding))

        integrated_reg_loss = first_reg_loss + second_reg_loss
        if not use_SVDpp:
            third_reg_loss = third_reg * (torch.norm(item_corr_weight) + torch.norm(item_corr_offset))
            integrated_reg_loss += third_reg_loss
        return integrated_reg_loss


    loss_monitor = []
    best_prediction, best_loss = None, None
    for i in range(iters):
        optimizer.zero_grad()
        integrated_model = build_integrated_model(param_list, factor_normalizer)
        loss = torch.mean((integrated_model - data_matrix)[is_provided] **2) + build_regularization(first_reg, second_reg, third_reg, param_list)
        loss.backward()
        optimizer.step()
        scheduler.step()
        with torch.no_grad():
            item_corr_weight.fill_diagonal_(0)
            loss_monitor.append(loss.item())
            if best_loss is None or loss < best_loss:
                best_prediction = integrated_model.clone().detach()
                best_loss = loss.item()
        

    prediction = best_prediction.cpu().detach().numpy()
    return prediction, loss_monitor

def impute_by_Topic_Model(data_matrix, is_provided, num_epoch:int, num_topic:int, dim_embed:int):
    '''
    Implements topic model; steps are computed via gradient decent instead of EM algorithm.
    '''
    # intialize
    from tqdm import tqdm
    num_user, num_item = data_matrix.shape
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    print("Using:", device)

    V = torch.rand(num_topic, num_item, device=device)
    user_embedding = torch.rand(dim_embed, num_user, device=device)
    topic_embedding = torch.rand(5*dim_embed, num_topic, device=device)
    data_matrix, is_provided = torch.from_numpy(data_matrix).to(device), torch.from_numpy(is_provided).to(device)

    optimizer = torch.optim.Adam([user_embedding, topic_embedding, V], lr=1e-2)
    loss_monitor = []
    for epoch in tqdm(range(num_epoch)):
        V.requires_grad = True
        user_embedding.requires_grad = True
        topic_embedding.requires_grad = True
        optimizer.zero_grad()
        loss = torch.tensor(0, dtype = torch.float, device = device)
        V_softmax = torch.nn.functional.softmax(V, dim = 0)
        item_embedding = torch.matmul(topic_embedding, V_softmax)
        
        user_item_rank = torch.zeros([num_user, num_item, 5], dtype=torch.float64, device=device)
        for i in range(5):
            user_item_rank[:, :, i] = torch.matmul(user_embedding.T, item_embedding[i*dim_embed:(i+1)*dim_embed, :])
        user_item_rank_log_prob = torch.log(torch.nn.functional.softmax(user_item_rank, dim = 2))
        
        for i in range(5):
            loss = loss - torch.sum(user_item_rank_log_prob[:,:,i][data_matrix==(i+1)])

        loss.backward()
        optimizer.step()
        loss_monitor.append(loss.item())
    

    V_softmax = torch.nn.functional.softmax(V, dim = 0)
    item_embedding = torch.matmul(topic_embedding, V_softmax)
    pred = torch.zeros([num_user, num_item], dtype=torch.float64, device=device)
    user_item_rank = torch.zeros([num_user, num_item, 5], dtype=torch.float64, device=device)
    for i in range(5):
        user_item_rank[:, :, i] = torch.matmul(user_embedding.T, item_embedding[i*dim_embed:(i+1)*dim_embed, :])
    user_item_rank_prob = torch.nn.functional.softmax(user_item_rank, dim = 2)

    for i in range(5):
        pred[~is_provided] += (i+1)*user_item_rank_prob[:,:, i][~is_provided]

    pred_clip = data_matrix
    pred_clip[~is_provided] = pred[~is_provided].to(torch.int8)

    return pred_clip.detach().cpu().numpy(), loss_monitor
