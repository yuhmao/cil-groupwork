import pandas as pd
import numpy as np
from sklearn.tree import DecisionTreeRegressor
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.ensemble import GradientBoostingRegressor
from xgboost import XGBRegressor

import torch
import torch.nn as nn
from torch.utils.data import DataLoader, TensorDataset
import torch.nn.functional as F

from util import read_data_in_dense_matrix, eval_prediction, clip_prediction, store_dense_matrix_to_submission, convert_matrix_to_df, get_test
from baselines import impute_by_Koren_three_facet, impute_by_ALS_GD, impute_by_nuclear_relaxation, impute_by_SVP, impute_by_mean
from our_methods import impute_by_DiscreteLabelExpectation, impute_by_DiscreteLabelClassification, impute_by_DiscreteLabelCombined
from neural_methods import impute_by_DMF
from bayesian_methods import impute_by_Bayesian

# train SVD++


def SVDPP(train_matrix, is_train, params):
    first_reg = params['first_reg']
    second_reg = params['second_reg']
    dim_embed = params['dim_embed']
    iters = params['iters']

    prediction, loss_monitor = impute_by_Koren_three_facet(
        train_matrix, is_train, dim_embed=dim_embed, iters=iters, first_reg=first_reg, second_reg=second_reg,
        use_SVDpp=True)

    return prediction


# train ALS


def ALS(train_matrix, is_train, params):
    k = params['k']
    reg = params['reg']

    prediction, loss_monitor = impute_by_ALS_GD(
        train_matrix, is_train, num_epoch=1000, iters_per_epoch=20, rank=k, reg_weight=reg)
    return prediction


# train nuclear


def Nuclear(train_matrix, is_train, params):
    best_iter = params['best_iter']
    reg = params['reg']

    prediction = impute_by_nuclear_relaxation(
        train_matrix, is_train, iters=best_iter, nuclear_reg=reg, start_from=None)

    return prediction


# train SVP


def SVP(train_matrix, is_train, params):
    best_iter = params['best_iter']
    rank = params['rank']

    prediction = impute_by_SVP(
        train_matrix, is_train, iters=best_iter, rank=rank, start_from=None)
    return prediction


# train DMF


def DMF(train_matrix, is_train, pararms):
    reg = pararms['reg']
    dim_embed = pararms['dim_embed']

    prediction, loss_monitor = impute_by_DMF(
        train_matrix, is_train, iters=2000, dim_embed=dim_embed, reg_user=reg, reg_item=reg, lr=1e-4)

    return prediction


def DLE(train_matrix, is_train, params):
    reg = params['reg']
    dim_embed = params['dim_embed']
    prior_reg = params['prior_reg']

    prediction, loss_monitor, param_list = impute_by_DiscreteLabelExpectation(
        train_matrix, is_train, dim_embed=dim_embed, iters=3000, reg=reg, prior_reg=prior_reg)

    return prediction


def DLC(train_matrix, is_train, params):
    reg = params['reg']
    dim_embed = params['dim_embed']
    prediction, loss_monitor, param_list = impute_by_DiscreteLabelClassification(train_matrix, is_train,dim_embed=dim_embed, iters=2000, reg=reg)

    return prediction

def DLCombined(train_matrix, is_train, params):
    reg = params["reg"]
    dim_embed = params["dim_embed"]
    alpha = params["alpha"]
    prediction, loss_monitor, param_list = impute_by_DiscreteLabelCombined(train_matrix, is_train, dim_embed=dim_embed, iters=3000, reg=reg, alpha=alpha)
    return prediction

def MEAN(train_matrix, is_train, params):
    mean_type = params["mean_type"]
    prediction = impute_by_mean(train_matrix, is_train, mean_type=mean_type)
    return prediction

def Bayesian(train_matrix, is_train, test_matrix, is_test, params):
    train_matrix = np.copy(train_matrix)
    test_matrix = np.copy(test_matrix)
    train_matrix[~is_train] = 0
    test_matrix[~is_test] = 0
    train_df = convert_matrix_to_df(train_matrix)
    test_df = convert_matrix_to_df(test_matrix)
    pred_data = get_test('../../data/sampleSubmission.csv')
    # concat all the entries we want to know
    pred_data = pd.concat([pred_data, train_df[["user_id", "movie_id"]], test_df[["user_id", "movie_id"]]])
    prediction = impute_by_Bayesian(train_df, test_df, pred_data, **params)
    return prediction

def run_prediction(pred_dict, train_matrix, is_train, test_matrix, is_test):
    method = pred_dict['method']
    params = pred_dict['params']

    print("Running {}...".format(pred_dict['method']))
    result = None

    if method == 'SVDPP':
        result = SVDPP(train_matrix, is_train, params)
    elif method == 'ALS':
        result = ALS(train_matrix, is_train, params)
    elif method == 'Nuclear':
        result = Nuclear(train_matrix, is_train, params)
    elif method == 'SVP':
        result = SVP(train_matrix, is_train, params)
    elif method == 'DMF':
        result = DMF(train_matrix, is_train, params)
    elif method == 'DLE':
        result = DLE(train_matrix, is_train, params)
    elif method == 'DLC':
        result = DLC(train_matrix, is_train, params)
    elif method == "DLCombined":
        result = DLCombined(train_matrix, is_train, params)
    elif method == "MEAN":
        result = MEAN(train_matrix, is_train, params)
    elif method == "Bayesian":
        result = Bayesian(train_matrix, is_train, test_matrix, is_test, params)
    else:
        raise NotImplementedError("Unknown method {}".format(method))
    
    # import pdb;pdb.set_trace()
    train_rmse, test_rmse = eval_prediction(
        result, train_matrix, is_train), eval_prediction(result, test_matrix, is_test)
    print(
        f"Method: {method}, Train RMSE: {train_rmse:.4f}, Test RMSE: {test_rmse:.4f}, Mean: {result.mean():.4f}, StdDev: {result.std():.4f}")
    return result


########################################################################################################
# Ensembles

def get_param(params: dict, name: str, default_val):
    return params[name] if name in params else default_val


def equal_mean(X_train, Y_train, X_ensemble, mat_shape, params):
    ensemble = X_ensemble.mean(axis=1).reshape(mat_shape)
    return ensemble


# Do not use this: least square should be better and faster than gradient descent.


def linear_gradient_descent(train_matrix, is_train, test_matrix, is_test, pred_dict, reg: float, lr: float):
    predictions = list(pred_dict.values())
    num_models = len(predictions)

    weights = torch.ones(num_models + 1, 1, 1)  # The last item is the bias
    weights[0:num_models] = 1 / num_models
    weights.requires_grad = True

    predictions = torch.from_numpy(np.stack(predictions))
    optimizer = torch.optim.Adam([weights, ], lr=lr)
    # scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, [50,], gamma=0.2)
    is_train_torch = torch.from_numpy(is_train)
    loss_monitor = []
    for _ in range(100):
        optimizer.zero_grad()
        # Note that we make the weights of all models sum to 1
        ensemble = (weights[0:num_models] / weights[0:num_models].sum()
                    * predictions + weights[num_models]).sum(dim=0)
        loss = ((ensemble[is_train_torch] -
                 torch.from_numpy(train_matrix)[is_train_torch]) ** 2).mean()
        loss += reg * (weights ** 2).sum()
        loss.backward()
        optimizer.step()
        # scheduler.step()
        loss_monitor.append(loss.item())

    ensemble = (weights[0:num_models] / weights[0:num_models].sum()
                * predictions + weights[num_models]).sum(dim=0).detach().numpy()
    train_rmse, test_rmse = eval_prediction(
        ensemble, train_matrix, is_train), eval_prediction(ensemble, test_matrix, is_test)
    print(f"Train RMSE: {train_rmse:.4f}, Test RMSE: {test_rmse:.4f}")
    return ensemble


def linear_least_square(X_train, Y_train, X_ensemble, mat_shape, params):
    bias_term = get_param(params, 'bias_term', True)

    if bias_term:
        X_train = np.concatenate(
            (X_train, np.ones((X_train.shape[0], 1))), axis=1)
        X_ensemble = np.concatenate(
            (X_ensemble, np.ones((X_ensemble.shape[0], 1))), axis=1)

    weights = np.linalg.lstsq(X_train, Y_train)[0]
    # weights = scipy.optimize.lsq_linear(X_train, Y_train, (0.0, np.inf))['x']
    # For debugging: print weights
    # print(weights)

    Y_ensemble = X_ensemble @ weights
    ensemble = Y_ensemble.reshape(mat_shape)
    return ensemble


def ridge(X_train, Y_train, X_ensemble, mat_shape, params):
    bias_term = get_param(params, 'bias_term', True)

    if bias_term:
        X_train = np.concatenate(
            (X_train, np.ones((X_train.shape[0], 1))), axis=1)
        X_ensemble = np.concatenate(
            (X_ensemble, np.ones((X_ensemble.shape[0], 1))), axis=1)

    alpha = get_param(params, 'alpha', 100.0)

    regr = Ridge(alpha=alpha, positive=False)
    regr.fit(X_train, Y_train)
    # For debugging: print weights
    # print(regr.coef_)
    # print(sum(regr.coef_))

    Y_ensemble = regr.predict(X_ensemble)
    ensemble = Y_ensemble.reshape(mat_shape)

    return ensemble


def decision_tree(X_train, Y_train, X_ensemble, mat_shape, params):
    max_depth = get_param(params, 'max_depth', 5)

    regr = DecisionTreeRegressor(max_depth=max_depth)
    regr.fit(X_train, Y_train)

    Y_ensemble = regr.predict(X_ensemble)
    ensemble = Y_ensemble.reshape(mat_shape)

    return ensemble


def xgboost(X_train, Y_train, X_ensemble, mat_shape, params):
    n_estimators = get_param(params, 'n_estimators', 200)
    subsample = get_param(params, 'subsample', 0.75)
    learning_rate = get_param(params, 'learning_rate', 0.1)
    max_depth = get_param(params, 'max_depth', 4)
    colsample_bytree = get_param(params, 'colsample_bytree', 1.0)
    colsample_bylevel = get_param(params, 'colsample_bylevel', 0.1)

    regr = XGBRegressor(n_estimators=n_estimators, subsample=subsample, learning_rate=learning_rate,
                        max_depth=max_depth, colsample_bytree=colsample_bytree, colsample_bylevel=colsample_bylevel)
    regr.fit(X_train, Y_train)

    Y_ensemble = regr.predict(X_ensemble)
    ensemble = Y_ensemble.reshape(mat_shape)

    return ensemble


def gbdt(X_train, Y_train, X_ensemble, mat_shape, params):
    n_estimators = get_param(params, 'n_estimators', 150)
    subsample = get_param(params, 'subsample', 0.75)
    learning_rate = get_param(params, 'learning_rate', 0.1)
    max_depth = get_param(params, 'max_depth', 4)
    max_features = get_param(params, 'max_features', 2)

    regr = GradientBoostingRegressor(n_estimators=n_estimators, subsample=subsample, learning_rate=learning_rate,
                                     max_depth=max_depth, max_features=max_features)
    regr.fit(X_train, Y_train)

    Y_ensemble = regr.predict(X_ensemble)
    ensemble = Y_ensemble.reshape(mat_shape)

    return ensemble

class nn_model(nn.Module):
    def __init__(self, n_predictions, bias_term):
        super().__init__()

        self.n_predictions = n_predictions

        self.model = nn.Sequential(
            nn.Linear(in_features=self.n_predictions, out_features=self.n_predictions//2, bias=bias_term),
            nn.BatchNorm1d(self.n_predictions//2),
            nn.Tanh(),
            nn.Linear(in_features=self.n_predictions//2, out_features=self.n_predictions//4, bias=bias_term),
            nn.BatchNorm1d(self.n_predictions//4),
            nn.Tanh(),
            # nn.Linear(in_features=self.n_predictions//2, out_features=self.n_predictions//4, bias=bias_term),
            # nn.BatchNorm1d(self.n_predictions//4),
            # nn.Tanh(),
            # nn.Linear(in_features=self.n_predictions//4, out_features=self.n_predictions//8, bias=bias_term),
            # nn.BatchNorm1d(self.n_predictions//8),
            # nn.Tanh(),
            # nn.Linear(in_features=self.n_predictions//2, out_features=self.n_predictions//4, bias=bias_term),
            # nn.BatchNorm1d(self.n_predictions//4),
            # nn.Tanh(),
            # nn.Linear(in_features=self.n_predictions//2, out_features=self.n_predictions//4, bias=bias_term),
            # nn.Tanh(),
            # nn.Linear(in_features=self.n_predictions//4, out_features=self.n_predictions//8, bias=bias_term),
            # nn.ReLU(),
            nn.Linear(in_features=self.n_predictions//4, out_features=1, bias=True)
        )

        self.init_weights()

    def forward(self, X_ensemble):
        return self.model(X_ensemble)
        # return self.model(X_ensemble) * (5.0-1.0) + 1.0

    def init_weights(self):
        def init_fn(m):
            if isinstance(m, nn.Linear):
                # torch.nn.init.kaiming_normal_(m.weight, nonlinearity='relu')
                # m.bias.data.fill_(0.0)
                torch.nn.init.xavier_normal_(m.weight)
                # m.weight.data.fill_(1.0/m.weight.size(1))
                # m.bias.data.fill_(0.0)

        self.model.apply(init_fn)


def nn_ensemble(X_train, Y_train, X_test, Y_test, X_ensemble, mat_shape, params):
    batch_size = 128
    num_epochs = 100
    learning_rate = 1e-4
    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
    n_predictions = X_train.shape[1]


    bias_term = get_param(params, 'bias_term', False)

    train_loader = DataLoader(
        TensorDataset(torch.Tensor(X_train), torch.Tensor(Y_train)),
        batch_size=batch_size, shuffle=True
    )

    nn_model_instance = nn_model(n_predictions, bias_term).to(device)
    # def init_weights(m):
    #     if isinstance(m, nn.Linear):
    #
    # nn_model_instance.apply(lambda m : )

    optimizer = torch.optim.Adam(nn_model_instance.parameters(), lr=learning_rate)
    loss_fn = nn.MSELoss()

    # Train the model

    X_test = torch.Tensor(X_test).to(device)
    for epoch in range(num_epochs):
        train_loss = 0.0
        nn_model_instance.train()

        for batch in train_loader:
            X_batch, Y_batch = batch
            X_batch = X_batch.to(device)
            Y_batch = Y_batch.reshape(-1, 1).to(device)

            optimizer.zero_grad()
            outputs = nn_model_instance(X_batch)
            loss = loss_fn(outputs, Y_batch)
            loss.backward()
            optimizer.step()

            train_loss += loss.item()

        train_loss = np.sqrt(train_loss / len(train_loader))

        nn_model_instance.eval()
        Y_test_pred = nn_model_instance(X_test).cpu().detach().numpy().flatten()
        Y_test_pred = clip_prediction(Y_test_pred)

        test_loss = np.sqrt(np.mean(np.square(Y_test - Y_test_pred)))
        print(f'Epoch {epoch + 1}/{num_epochs}, Train RMSE: {train_loss:.4f}, Test RMSE: {test_loss:.4f}')

    # Ensemble results
    nn_model_instance.eval()
    X_ensemble = torch.Tensor(X_ensemble).to(device)
    Y_ensemble = nn_model_instance(X_ensemble).cpu().detach().numpy().flatten()
    ensemble = Y_ensemble.reshape(mat_shape)

    return ensemble




def run_ensemble(ens_dict, predictions, ensemble_train_matrix, is_ensemble_train, ensemble_test_matrix,
                 is_ensemble_test, pre_clip):
    mat_shape = ensemble_train_matrix.shape


    # X shape=(n_samples, n_predictions)
    X_train = np.stack([pred_dict['result'][is_ensemble_train]
                        for pred_dict in predictions], axis=-1)

    # Y shape=(n_samples,)
    Y_train = ensemble_train_matrix[is_ensemble_train]

    X_test = np.stack([pred_dict['result'][is_ensemble_test]
                        for pred_dict in predictions], axis=-1)

    Y_test = ensemble_test_matrix[is_ensemble_test]

    X_ensemble = np.stack([pred_dict['result'].flatten()
                           for pred_dict in predictions], axis=-1)

    if pre_clip:
        X_train = clip_prediction(X_train)
        X_test = clip_prediction(X_test)
        X_ensemble = clip_prediction(X_ensemble)


    method = ens_dict['method']
    params = ens_dict['params']

    result = None

    if method == 'equal_mean':
        result = equal_mean(X_train, Y_train, X_ensemble, mat_shape, params)
    # if method == 'linear_gradient_descent':
    #    result = linear_gradient_descent(X_train, Y_train, X_ensemble, mat_shape)
    elif method == 'linear_least_square':
        result = linear_least_square(X_train, Y_train, X_ensemble, mat_shape, params)
    elif method == 'ridge':
        result = ridge(X_train, Y_train, X_ensemble, mat_shape, params)
    elif method == 'decision_tree':
        result = decision_tree(X_train, Y_train, X_ensemble, mat_shape, params)
    elif method == 'xgboost':
        result = xgboost(X_train, Y_train, X_ensemble, mat_shape, params)
    elif method == 'gbdt':
        result = gbdt(X_train, Y_train, X_ensemble, mat_shape, params)
    elif method == 'nn_ensemble':
        result = nn_ensemble(X_train, Y_train, X_test, Y_test, X_ensemble, mat_shape, params)
    else:
        raise NotImplementedError("Unknown method {}".format(method))

    result = clip_prediction(result)

    train_rmse, test_rmse = eval_prediction(result, ensemble_train_matrix, is_ensemble_train), eval_prediction(result,
                                                                                                               ensemble_test_matrix,
                                                                                                               is_ensemble_test)
    print(
        f"Method: {method}, Train RMSE: {train_rmse:.4f}, Test RMSE: {test_rmse:.4f}")
    return result
