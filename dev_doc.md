# Doc for development

For any functionality, wrap it properly and indicate its usage here.

Guidelines:

- For any bug report, please inform other gourp members first.
- Do not change the code written by others. If a code revision is requested, please indicate why and inform others.
- If you feel the doc for a function is not complete enough, please inform its author.
- For any function added, please test its correctness first. You may provide validation examples of the function whenever possible. Make sure its name properly indicates its functionality.
- Write complete usage instructions in the doc.
- Do **not** doc low-level helper functions. Only include major functionalities, e.g., implementation of a baseline method, data reading and processing.
- While you should only doc high-level functions, please keep good coding style, i.e., split the steps in this functionality into a series of low-level functions. You may start these low-level functions by ```_```, e.g. ```_read_df```. That being said, you are required to constantly update or split the code blocks if a new feature support is added in your high-level function. Respect the "atom function" principle, i.e., a low-level function should try to do only one thing. In addition, a function that will be frequently used in the development of other functionalities should be considered high-level and wrapped separately.
- Indicate the contact of each functionality. You may use nicknames. A good choice is your Github username.
- Provide the estimated time cost in the beginning of the function.
- **Important**: coding is communication. If you feel the coding style of a function is hard to read, please inform the author and provide suggestions and discussions so that we all improve our coding skills in this project. 

Feel free to add guidlines if you feel appropriate.

The full list of nicknames (add yours here):

1. AlgebraLoveme


## Data Processing

### Read Data in the Format of Dense Matrix

Author: AlgebraLoveme

Time Estimate: ~3.5 sec

This function allows end-to-end reading of the data. It returns a dense matrix, in the term of two dimensional numpy array. The entry equals the ground truth, otherwise equals zero if the corresponding ground truth is missing. It also provides train/test split by ```test_size``` flag.

The size of the array is (10000, 1000). The data type is ```np.int8```.
```
from util import read_data_in_dense_matrix

data_matrix, is_provided = read_data_in_dense_matrix('../data/data_train.csv', n_col=1000, n_row=10000)
```

There are 10000 users and 1000 items in the data. To access user $i$ and item $j$, use ```data_matrix[i-1,j-1]```. This is because the labels of the users and the items start from 1.

A boolean mask matrix of the same shape of the data matrix is also returned (```is_provided```). The mask is true for the entries if and only if a ground truth for the entries is provided.

By default, you can omit n_row and n_col. They will be infered by using the maximum index in the data. However, if the data is split before converting to data matrix due to the usage of ```test_size```, it is helpful to specify the n_row and n_col you want to use.

Test usage:
```
# The first line of the train data is:
# Id,Prediction
# r44_c1,4

data_matrix[43, 0]
# The result is 4.

data_matrix[42, 0]
# The result is 0.

is_provided[43, 0]
# The result is True.

is_provided[42, 0]
# The result is False.
```

To enable the split of train set and test set, use ```test_size``` flag, which means the ratio of the test set, as follows:
```
(train_matrix, test_matrix), (is_train, is_test) = read_data_in_dense_matrix('../data/data_train.csv', n_col=1000, n_row=10000, test_size=0.2)

train_matrix.shape, test_matrix.shape, is_train.shape, is_test.shape, np.sum(is_train), np.sum(is_test), np.sum(is_train & is_test)
# ((10000, 1000), (10000, 1000), (10000, 1000), (10000, 1000), 941561, 235391, 0)
```
This easily splits the non-overlap train set and test set.

## Baselines

### Impute by mean

Author: AlgebraLoveme

Time Estimate: ~0.1 sec

Replace all unknown values by the mean of row/col.

```
from baselines import impute_by_mean

prediction = impute_by_mean(train_matrix, is_train, mean_type='col')

eval_prediction(prediction, train_matrix, is_train, eval_metric='rmse')
# The result is 0.0
eval_prediction(prediction, test_matrix, is_test, eval_metric='rmse')
# The result is 1.0292941184259519


prediction = impute_by_mean(train_matrix, is_train, mean_type='row')

eval_prediction(prediction, train_matrix, is_train, eval_metric='rmse')
# The result is 0.0
eval_prediction(prediction, test_matrix, is_test, eval_metric='rmse')
# The result is 1.0952974128408826

```

### Impute by SVD

Author: zhaoan

Time Estimate: ~0.1 sec

Replace all unknown values by singular value decomposition (SVD). Use 90% of total energy for matrix Sig2 as the threshold to choose the reduce dimensions.

```
from baselines import impute_by_svd

prediction = impute_by_svd(train_matrix)

eval_prediction(prediction, train_matrix, is_train, eval_metric='rmse')
# The result is 
```

## Performance Evaluation

The evaluation function is ```eval_prediction(prediction, test_matrix, is_test, eval_metric='rmse')``` in ```util.py```. For any development of other metrics, please incorporate it into this wrapper function with a corresponding ```eval_metric``` flag. 

### Evaluate by RMSE

Author: AlgebraLoveme

Time Estimate: ~0.1 sec

The RMSE score is computed by 

$$RMSE = \sqrt{\frac{\sum_{(i,j)\in \Omega} (X^{pred}_{i,j} - X^{test}_{i,j})^2}{\sum_{(i,j)\in \Omega} 1}}$$

```
from util import eval_prediction
from baselines import impute_by_mean

prediction = impute_by_mean(train_matrix, is_train, mean_type='col')

eval_prediction(prediction, train_matrix, is_train, eval_metric='rmse')
# The result is 0.0
eval_prediction(prediction, test_matrix, is_test, eval_metric='rmse')
# The result is 1.0292941184259519
```

