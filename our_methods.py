import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from util import move_param_to_device

def impute_by_DiscreteLabelExpectation(data_matrix, is_provided, dim_embed:int, iters:int, reg:float, prior_reg:float=0, num_class:int=5):
    '''
    Impute via using the expectation of discrete labels.
    '''

    num_user, num_item = data_matrix.shape
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    print("Using:", device)

    data_matrix, is_provided = torch.from_numpy(data_matrix).to(device), torch.from_numpy(is_provided).to(device)

    item_embedding_explicit = torch.rand(num_class, num_item, dim_embed)
    item_embedding_implicit = torch.rand(num_class, num_item, dim_embed)
    user_embedding = torch.rand(num_class, num_user, dim_embed)


    param_list = [item_embedding_explicit, item_embedding_implicit, user_embedding]
    move_param_to_device(param_list, device)
    optimizer = torch.optim.Adam(param_list, lr=1e-2)
    scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=[iters//8 * i for i in range(3,7)], gamma=0.2)
    factor_normalizer = torch.sqrt(is_provided.sum(dim=1, keepdim=True)).to(device)

    def build_integrated_model(param_list, factor_normalizer):
        item_embedding_explicit, item_embedding_implicit, user_embedding = param_list

        user_embedding = user_embedding + torch.matmul(is_provided.float().unsqueeze(0), item_embedding_implicit) / factor_normalizer.unsqueeze(0)
        logits = torch.matmul(user_embedding, torch.transpose(item_embedding_explicit, dim0=1, dim1=2))

        # the probability of falling into each class
        probs = F.softmax(logits, dim=0)
        cond_mean = (1+torch.arange(num_class).view(-1,1,1)).to(device) * probs
        integrated_model = torch.sum(cond_mean, dim=0)

        # priors: should have concentrated probs
        # TODO: change this
        prior_loss = torch.var(probs, dim=0).mean()

        return integrated_model, prior_loss, logits

    def build_regularization(reg, param_list):
        item_embedding_explicit, item_embedding_implicit, user_embedding= param_list
        reg_loss = reg * (torch.norm(item_embedding_explicit) + torch.norm(item_embedding_implicit) + torch.norm(user_embedding))

        integrated_reg_loss = reg_loss

        return integrated_reg_loss


    loss_monitor = []
    best_prediction, best_loss = None, None
    for i in range(iters):
        optimizer.zero_grad()
        integrated_model, prior_loss, logits = build_integrated_model(param_list, factor_normalizer)
        loss = build_regularization(reg, param_list)
        loss += torch.mean((integrated_model - data_matrix)[is_provided] **2)

        loss.backward()
        optimizer.step()
        scheduler.step()
        with torch.no_grad():
            # item_corr_weight.fill_diagonal_(0)
            loss_monitor.append(loss.item())
            if best_loss is None or loss < best_loss:
                best_prediction = integrated_model.clone().detach()
                best_loss = loss.item()


    prediction = best_prediction.cpu().detach().numpy()
    return prediction, loss_monitor, param_list



def impute_by_DiscreteLabelClassification(data_matrix, is_provided, dim_embed:int, iters:int, reg:float, prior_reg:float=0, num_class:int=5):
    '''
    Very similar to Discrete Label Expectation, but use classification model instead of regression.
    '''

    num_user, num_item = data_matrix.shape
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    print("Using:", device)

    data_matrix, is_provided = torch.from_numpy(data_matrix).to(device), torch.from_numpy(is_provided).to(device)

    item_embedding_explicit = torch.rand(num_class, num_item, dim_embed)
    item_embedding_implicit = torch.rand(num_class, num_item, dim_embed)
    user_embedding = torch.rand(num_class, num_user, dim_embed)


    param_list = [item_embedding_explicit, item_embedding_implicit, user_embedding]
    move_param_to_device(param_list, device)
    optimizer = torch.optim.Adam(param_list, lr=1e-2)
    scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=[iters//8 * i for i in range(3,7)], gamma=0.2)
    factor_normalizer = torch.sqrt(is_provided.sum(dim=1, keepdim=True)).to(device)

    def build_integrated_model(param_list, factor_normalizer):
        item_embedding_explicit, item_embedding_implicit, user_embedding = param_list

        user_embedding = user_embedding + torch.matmul(is_provided.float().unsqueeze(0), item_embedding_implicit) / factor_normalizer.unsqueeze(0)
        logits = torch.matmul(user_embedding, torch.transpose(item_embedding_explicit, dim0=1, dim1=2))

        # the probability of falling into each class
        probs = F.softmax(logits, dim=0)
        cond_mean = (1+torch.arange(num_class).view(-1,1,1)).to(device) * probs
        integrated_model = torch.sum(cond_mean, dim=0)

        # priors: should have concentrated probs
        # TODO: change this
        prior_loss = torch.var(probs, dim=0).mean()

        return integrated_model, prior_loss, logits

    def build_regularization(reg, param_list):
        item_embedding_explicit, item_embedding_implicit, user_embedding= param_list
        reg_loss = reg * (torch.norm(item_embedding_explicit) + torch.norm(item_embedding_implicit) + torch.norm(user_embedding))

        integrated_reg_loss = reg_loss

        return integrated_reg_loss


    loss_monitor = []
    best_prediction, best_loss = None, None
    for i in range(iters):
        optimizer.zero_grad()
        integrated_model, prior_loss, logits = build_integrated_model(param_list, factor_normalizer)
        loss = build_regularization(reg, param_list)
        logits = torch.transpose(torch.transpose(logits, 0, 1), 1, 2)
        x = logits[is_provided]
        y = data_matrix[is_provided].to(torch.long) - 1
        # weight = (integrated_model[is_provided] - y) **2
        # loss += (F.cross_entropy(x, y, reduction='none') * weight).mean()
        loss += F.cross_entropy(x, y)

        loss.backward()
        optimizer.step()
        scheduler.step()
        with torch.no_grad():
            loss_monitor.append(loss.item())
            if best_loss is None or loss < best_loss:
                best_prediction = integrated_model.clone().detach()
                best_loss = loss.item()


    prediction = best_prediction.cpu().detach().numpy()
    return prediction, loss_monitor, param_list



def impute_by_DiscreteLabelCombined(data_matrix, is_provided, dim_embed:int, iters:int, reg:float, alpha:float=0.5, prior_reg:float=0, num_class:int=5, pre_iter:int=20):
    '''
    Very similar to Discrete Label Expectation, but use classification model instead of regression.
    '''
    assert 0 < alpha < 1, "alpha should be in (0,1)." 

    num_user, num_item = data_matrix.shape
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    print("Using:", device)

    data_matrix, is_provided = torch.from_numpy(data_matrix).to(device), torch.from_numpy(is_provided).to(device)

    item_embedding_explicit = torch.rand(num_class, num_item, dim_embed)
    item_embedding_implicit = torch.rand(num_class, num_item, dim_embed)
    user_embedding = torch.rand(num_class, num_user, dim_embed)


    param_list = [item_embedding_explicit, item_embedding_implicit, user_embedding]
    move_param_to_device(param_list, device)
    optimizer = torch.optim.Adam(param_list, lr=1e-2)
    scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=[iters//8 * i for i in range(3,7)], gamma=0.2)
    factor_normalizer = torch.sqrt(is_provided.sum(dim=1, keepdim=True)).to(device)

    def build_integrated_model(param_list, factor_normalizer):
        item_embedding_explicit, item_embedding_implicit, user_embedding = param_list

        user_embedding = user_embedding + torch.matmul(is_provided.float().unsqueeze(0), item_embedding_implicit) / factor_normalizer.unsqueeze(0)
        logits = torch.matmul(user_embedding, torch.transpose(item_embedding_explicit, dim0=1, dim1=2))

        # the probability of falling into each class
        probs = F.softmax(logits, dim=0)
        cond_mean = (1+torch.arange(num_class).view(-1,1,1)).to(device) * probs
        integrated_model = torch.sum(cond_mean, dim=0)

        # priors: should have flat probs in the beginning
        prior_loss = torch.var(probs, dim=0).mean()

        return integrated_model, prior_loss, logits

    def build_regularization(reg, param_list):
        item_embedding_explicit, item_embedding_implicit, user_embedding= param_list
        reg_loss = reg * (torch.norm(item_embedding_explicit) + torch.norm(item_embedding_implicit) + torch.norm(user_embedding))

        integrated_reg_loss = reg_loss

        return integrated_reg_loss


    loss_monitor = []
    best_prediction, best_loss = None, None
    for i in range(pre_iter+iters):
        optimizer.zero_grad()
        integrated_model, prior_loss, logits = build_integrated_model(param_list, factor_normalizer)
        if i >= pre_iter:
            loss = build_regularization(reg, param_list)
            logits = torch.transpose(torch.transpose(logits, 0, 1), 1, 2)
            x = logits[is_provided]
            y = data_matrix[is_provided].to(torch.long) - 1
            # weight = (integrated_model[is_provided] - y) **2
            # loss += (F.cross_entropy(x, y, reduction='none') * weight).mean()
            loss = loss + alpha * F.cross_entropy(x, y) + (1-alpha) * torch.mean((integrated_model - data_matrix)[is_provided] **2)
        else:
            loss = prior_loss

        loss.backward()
        optimizer.step()
        if i >= pre_iter:
            scheduler.step()
            with torch.no_grad():
                loss_monitor.append(loss.item())
                if best_loss is None or loss < best_loss:
                    best_prediction = integrated_model.clone().detach()
                    best_loss = loss.item()


    prediction = best_prediction.cpu().detach().numpy()
    return prediction, loss_monitor, param_list
