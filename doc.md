# Doc for development

For any functionality, wrap it properly and indicate its usage here.

Guidelines:

- For any bug report, please inform other gourp members first.
- Do not change the code written by others. If a code revision is requested, please indicate why and inform others.
- If you feel the doc for a function is not complete enough, please inform its author.
- For any function added, please test its correctness first. You may provide validation examples of the function whenever possible. Make sure its name properly indicates its functionality.
- Write complete usage instructions in the doc.
- Do **not** doc low-level helper functions. Only include major functionalities, e.g., implementation of a baseline method, data reading and processing.
- While you should only doc high-level functions, please keep good coding style, i.e., split the steps in this functionality into a series of low-level functions. You may start these low-level functions by ```_```, e.g. ```_read_df```. That being said, you are required to constantly update or split the code blocks if a new feature support is added in your high-level function. Respect the "atom function" principle, i.e., a low-level function should try to do only one thing. In addition, a function that will be frequently used in the development of other functionalities should be considered high-level and wrapped separately.
- Indicate the contact of each functionality. You may use nicknames. A good choice is your Github username.
- Provide the estimated time cost in the beginning of the function.
- **Important**: coding is communication. If you feel the coding style of a function is hard to read, please inform the author and provide suggestions and discussions so that we all improve our coding skills in this project. 

Feel free to add guidlines if you feel appropriate.

The full list of nicknames (add yours here):

1. AlgebraLoveme
2. zhaoan

- [Doc for development](#doc-for-development)
  - [Data Processing](#data-processing)
    - [Read Data in the Format of Dense Matrix](#read-data-in-the-format-of-dense-matrix)
    - [Clip Prediction](#clip-prediction)
    - [Make Submission](#make-submission)
  - [Baselines](#baselines)
    - [Impute by mean](#impute-by-mean)
    - [Impute by SVD](#impute-by-svd)
    - [Impute by ALS (GD)](#impute-by-als-gd)
    - [Impute by SVP](#impute-by-svp)
    - [Impute by Nuclear Relaxation](#impute-by-nuclear-relaxation)
    - [Impute by Neural Collaborative Filtering (! development challenges)](#impute-by-neural-collaborative-filtering--development-challenges)
    - [Impute by Deep Matrix Factorization](#impute-by-deep-matrix-factorization)
    - [Impute by SVD++](#impute-by-svd-1)
    - [Impute by Bayesian SVD++](#impute-by-bayesian-svd)
  - [Takeaways](#takeaways)
  - [New Methods](#new-methods)
    - [Discrete Label Expectation](#discrete-label-expectation)
    - [Discrete Label Classification](#discrete-label-classification)
    - [Discrete Label Combined](#discrete-label-combined)
  - [Ensembles](#ensembles)
    - [SVD plus plus + SVP + ALS + nuclear relaxation](#svd-plus-plus--svp--als--nuclear-relaxation)
    - [previous + DMF](#previous--dmf)
  - [Hyperparameter Tuning](#hyperparameter-tuning)
    - [Binary Search with Automatic Overfitting detection](#binary-search-with-automatic-overfitting-detection)
  - [Performance Evaluation](#performance-evaluation)
    - [Evaluate by RMSE](#evaluate-by-rmse)


## Data Processing

### Read Data in the Format of Dense Matrix

Author: AlgebraLoveme

Time Estimate: ~2.8 sec

This function allows end-to-end reading of the data. It returns a dense matrix, in the term of two dimensional numpy array. The entry equals the ground truth, otherwise equals zero if the corresponding ground truth is missing. It also provides train/test split by ```test_size``` flag.

The size of the array is (10000, 1000). The data type is ```np.int8```.
```
from util import read_data_in_dense_matrix

data_matrix, is_provided = read_data_in_dense_matrix('../data/data_train.csv', n_col=1000, n_row=10000)
```

There are 10000 users and 1000 items in the data. To access user $i$ and item $j$, use ```data_matrix[i-1,j-1]```. This is because the labels of the users and the items start from 1. The data matrix is also known as **explicit feedback** in the literature.

A boolean mask matrix of the same shape of the data matrix is also returned (```is_provided```). The mask is true for the entries if and only if a ground truth for the entries is provided. The is_provided mask is also known as **implicit feedback** in the literature.

By default, you can omit n_row and n_col. They will be infered by using the maximum index in the data. However, if the data is split before converting to data matrix due to the usage of ```test_size```, it is helpful to specify the n_row and n_col you want to use.

Test usage:
```
# The first line of the train data is:
# Id,Prediction
# r44_c1,4

data_matrix[43, 0]
# The result is 4.

data_matrix[42, 0]
# The result is 0.

is_provided[43, 0]
# The result is True.

is_provided[42, 0]
# The result is False.
```

To enable the split of train set and test set, use ```test_size``` flag, which means the ratio of the test set, as follows:
```
(train_matrix, test_matrix), (is_train, is_test) = read_data_in_dense_matrix('../data/data_train.csv', n_col=1000, n_row=10000, test_size=0.2)

train_matrix.shape, test_matrix.shape, is_train.shape, is_test.shape, np.sum(is_train), np.sum(is_test), np.sum(is_train & is_test)
# ((10000, 1000), (10000, 1000), (10000, 1000), (10000, 1000), 941561, 235391, 0)
```
This easily splits the non-overlap train set and test set.


### Clip Prediction

Author: AlgebraLoveme

Time Estimate: instant

Clip your prediction to the range [1, 5]. By default, all methods should not clip their prediction to facilitate ensembles.

It is observed that in the weighted mean ensemble, do *not* apply clipping to the prediction pool, i.e., only clip the final prediction, gives a better score. This is because clipping wipes out additional out-of-scope preference of individual predictions in the pool.

```
from util import clip_prediction

prediction = clip_prediction(prediction) # equivalent to clip_prediction(prediction, 1, 5)
```

### Make Submission

Author: AlgebraLoveme

Time Estimate: ~18 sec

Make a submission file given the prediction. The given prediction will be automatically clipped to [1, 5].
```
# Make sure to use the full data to create submission, rather than only train data.

data_matrix, is_provided = read_data_in_dense_matrix('../data/data_train.csv', n_col=1000, n_row=10000, test_size=None)
prediction = impute_by_mean(data_matrix, is_provided, mean_type='col')

# the sampleSubmission file is given by the dataset, and is used to get all requests. The mysub file will be your submission, and can be changed to any other names.

store_dense_matrix_to_submission('../data/sampleSubmission.csv', '../data/mysub.csv', prediction) 

# Outputs:
# Loading requests specified by submission samples...
# Storing 1176952 records for submission as requested...
```

## Baselines

### Impute by mean

Author: AlgebraLoveme

Time Estimate: ~0.1 sec

Leadboard loss: 1.02982 (col mean), 1.09267 (row mean)

Test Code: ./test_codes/test_mean.ipynb

Replace all unknown values by the mean of row/col.

```
from baselines import impute_by_mean

prediction = impute_by_mean(train_matrix, is_train, mean_type='col')

eval_prediction(prediction, train_matrix, is_train, eval_metric='rmse')
# The result is 0.0
eval_prediction(prediction, test_matrix, is_test, eval_metric='rmse')
# The result is 1.0292941184259519


prediction = impute_by_mean(train_matrix, is_train, mean_type='row')

eval_prediction(prediction, train_matrix, is_train, eval_metric='rmse')
# The result is 0.0
eval_prediction(prediction, test_matrix, is_test, eval_metric='rmse')
# The result is 1.0952974128408826

```

### Impute by SVD

Author: zhaoan, AlgebraLoveme

Time Estimate: ~5 sec per iteration. Best iteration times estimated from train data is 26.

LeadBoard Loss: 0.99875

Test Code: ./test_codes/test_svd.ipynb

Start from a given matrix, perform SVD and use low rank approximation iteratively. The cut rank is determined by recovery precision threshold. The threshold can be specified via ```svd_keep_thre```, which is 0.9 by default. After fine-tuning, the LeadBoard Loss reaches minimum when the threshold approches to 0. After each iteration, the matrix is clipped to min=1 and max=5, and the train data is corrected.

```
from baselines import impute_by_svd

# If you want to evaluate intermediate results

rmse_dict = {}
num_iter = 50
for iter in range(num_iter):
    if iter == 0:
        prediction = impute_by_svd(train_matrix, is_train, 1, start_from=None)
    else:
        prediction = impute_by_svd(train_matrix, is_train, 1, start_from=prediction)

    rmse = eval_prediction(prediction, test_matrix, is_test)
    rmse_dict[iter] = rmse

# If only want the final result

iter = 26
prediction = impute_by_svd(data_matrix, is_provided, iter, start_from=None)

```

The loss-iter curve:

![SVD Result](./imgs/svd_curve.jpg)

### Impute by ALS (GD)

Author: AlgebraLoveme

Time Estimate: ~2 minutes on 1 RTX 2080Ti for 1000 epoch, 20 iter/epoch. Sufficient for convergence.

LeadBoard Loss: 0.98757

Test Code: ./test_codes/test_ALS.ipynb

Implements the alternative least square algorithm, but the least square is solved by gradient descent rather than solving the linear equation. This is to calculate the full matrix in parallel, as solving the linear equation is hard to parallize. By cross validation, the best rank is 3 and the best regularization weight is 0.9.

```
import matplotlib.pyplot as plt
from baselines import impute_by_ALS_GD


k, reg = 3, 0.9
prediction, loss_monitor = impute_by_ALS_GD(train_matrix, is_train, num_epoch=1000, iters_per_epoch=20, rank=k, reg_weight=reg)
plt.plot(loss_monitor)
plt.show()
# the loss_monitor stores the loss after each epoch; this plots the learning curve for convergence check
```

### Impute by SVP

Author: AlgebraLoveme

Time Estimate: ~4 minutes for 100 itertions.

LeadBoard Loss: 0.98714

Test Code: ./test_codes/test_SVP.ipynb

Implements the singular value projection algorithm. Starts from the column mean imputed matrix (without enforcing training data fitness) by default, then repeatly do gradient descent w.r.t. loss = $\|\Pi_\Omega(A_t - A)\|_F^2$ and project to low rank space by SVD after each step. You can specify the matrix to start, by using ```start_from```.

```
from baselines import impute_by_svd

# If you want to evaluate intermediate results

rmse_dict = {}
for iter in range(300):
    if iter == 0:
        prediction = impute_by_SVP(train_matrix, is_train, iters=1, rank=3, start_from=None)
    else:
        prediction = impute_by_SVP(train_matrix, is_train, iters=1, rank=3, start_from=prediction)
    rmse = eval_prediction(prediction, test_matrix, is_test)
    rmse_dict[iter] = rmse

# If only want the final result

best_iter = 206
prediction = impute_by_SVP(data_matrix, is_provided, iters=best_iter, rank=3, start_from=None)

```

### Impute by Nuclear Relaxation

Author: AlgebraLoveme

Time Estimate: ~2 minutes for 200 itertions.

LeadBoard Loss: 0.98212

Test Code: ./test_codes/test_nuclear_relax.ipynb

Implements the nuclear relaxation algorithm. It extracts every singular value by a threshold, and then clips them to min=0. By cross validation, the best clipping threshold is 3.5.

```
from baselines import impute_by_nuclear_relaxation

# If you want to evaluate intermediate results

rmse_dict = {}
reg = 3.5 # found by cross validation
for iter in range(300):
    if iter == 0:
        prediction = impute_by_nuclear_relaxation(train_matrix, is_train, iters=1, nuclear_reg=reg, start_from=None)
    else:
        prediction = impute_by_nuclear_relaxation(train_matrix, is_train, iters=1, nuclear_reg=reg, start_from=prediction)
    rmse = eval_prediction(prediction, test_matrix, is_test)
    rmse_dict[iter] = rmse

# If only want the final result

best_iter = 183
prediction = impute_by_nuclear_relaxation(data_matrix, is_provided, iters=best_iter, nuclear_reg=reg, start_from=None)


```

### Impute by Neural Collaborative Filtering (! development challenges)

Author: AlgebraLoveme

Test Code: ./test_codes/test_NCF.ipynb

!! Under development. The original paper (''Neural collaborative filtering.'' Proceedings of the 26th international conference on world wide web. 2017) is not well-suited as it only uses implicit feedback and binary prediction. The current implementation changes the loss to MSE, but finds that it basically has the same performance as the column mean imputation.

### Impute by Deep Matrix Factorization

Author: AlgebraLoveme

Time Estimate: ~39 sec for 2000 iterations on 1 RTX 2080Ti.

Leadboard Loss: 0.98145

Test Code: ./test_codes/test_DMF.ipynb

Reference: https://www.ijcai.org/proceedings/2017/0447.pdf

The original DMF considers ranking tasks. Implementation here only takes its main idea: use user/item explicit feedback (unknown interactions are filled with zeros) as the input of a neural network, and use the output of the network as their embeddings. The user embedding and item embedding are then dot producted to estimate the score, i.e., $\hat{R}_{ui} = f_\theta (R_{u,:})^\top f_\phi(R_{:,i})$, where $f_\theta$ and $f_\phi$ are neural networks with low dimensional output.

```
from neural_methods import impute_by_DMF

reg = 15
dim_embed = 13
prediction, loss_monitor = impute_by_DMF(train_matrix, is_train, iters=2000, dim_embed=dim_embed, reg_user=reg, reg_item=reg, lr=1e-4)
train_rmse, test_rmse = eval_prediction(prediction, train_matrix, is_train), eval_prediction(prediction, test_matrix, is_test)
print(f"Train RMSE: {train_rmse:.4f}, Test RMSE: {test_rmse:.4f}")
# Train RMSE: 0.9531, Test RMSE: 0.9865
```

### Impute by SVD++

Author: AlgebraLoveme

Time Estimate: ~30s for 3000 itertions.

LeadBoard Loss: 0.97898

Test Code: ./test_codes/test_Koren.ipynb

Reference: https://people.engr.tamu.edu/huangrh/Spring16/papers_course/matrix_factorization.pdf

Implements the Koren's integrated model. The integrated model has three-tier: 
1. general facet about global, user and item
2. iteraction between user and item
3. neighborhood info about item

The SVD++ model only uses the first two, i.e., $\hat{R}_{ui} = \mu + b_u + b_i + q_i^\top (p_u + \sum_{i \in R(u)} y_i)$.
 Combining the third tier does not show improvement on SVD++. Instead, adding the third tier model shows significant decrease on train RMSE, but does not improve test RMSE, suggesting overfitting.

```
from baselines import impute_by_Koren_three_facet

# The SVD++ model
first_reg = 0.001
second_reg = 0.0015
dim_embed = 14
prediction, loss_monitor = impute_by_Koren_three_facet(train_matrix, is_train, dim_embed=dim_embed, iters=3000, first_reg=first_reg, second_reg=second_reg, use_SVDpp=True)
# Train RMSE: 0.8972, Test RMSE: 0.9816

# The integrated model
third_reg = 0.015
k = 10
prediction, loss_monitor = impute_by_Koren_three_facet(train_matrix, is_train, dim_embed=dim_embed, iters=3000, first_reg=first_reg, second_reg=second_reg, third_reg=third_reg, k=k)
# Train RMSE: 0.8966, Test RMSE: 0.9975

```

### Impute by Bayesian SVD++
 
Author: zhaoan, AlgebraLoveme

Time Estimate: ~12min for 1000 iterations

Leadboard Loss: 0.97133

Reference: https://github.com/tohtsky/myFM
Prepare: pip install myfm

The implementation is adapted from myFM github examples/ml-100k-regression.py. 

'''
python ml-100k-regression.py
'''


## Takeaways

1. Generalization is more important than parametrization. Use stronger regularization if you use more parametrization. 

    Experiments: NCF is bad, but DMF is better.
2. Topic models may be beneficial (strongly recommended). 

   Experiments: SVD++ is better than SVD by simply including implicit feedback which only considers whether the user interacts with the item. Weighting the implicit feedback by ratings decreases the performance. Rationale: the user only interacts with intersted items, which should be suggested by the item's category. Topic models reduce the parametrization as well. 

   Reference (only considers implicit feedback, needs adaption): http://www.cs.columbia.edu/~blei/papers/WangBlei2011.pdf
3. Neighborhood info (items with similar ratings that the user interacts) is not useful. 

   Experiments: including the third tier in Koren's model is not changing the test performance.

## New Methods

### Discrete Label Expectation

Author: AlgebraLoveme

Time Estimate: ~1 min for 3000 iterations

Leadboard Loss: 0.98228

Test Code: ./test_codes/test_DLE.ipynb

Idea: use matrix factorization to predict the probability of each label, i.e., the ranking (1-5). The expectation of the labels, i.e. $\sum_{i=1}^{5} i\times p_i$ is used as the predicted score. MSE is used as the loss function.

```
from our_methods import impute_by_DiscreteLabelExpectation

reg = 0.0015
dim_embed = 3
prior_reg = 0
prediction, loss_monitor, param_list = impute_by_DiscreteLabelExpectation(train_matrix, is_train, dim_embed=dim_embed, iters=3000, reg=reg, prior_reg=prior_reg)
train_rmse, test_rmse = eval_prediction(prediction, train_matrix, is_train), eval_prediction(prediction, test_matrix, is_test)
print(f"Train RMSE: {train_rmse:.4f}, Test RMSE: {test_rmse:.4f}")
```

### Discrete Label Classification

Author: haoyin, AlgebraLoveme

Time Estimate: ~1min40s for 2000 iterations

Leadboard Loss: 0.97893

Test Code: ./test_codes/test_DLC.ipynb

Idea: similar to DLE, we use matrix factorization to predict the probability of each label. However, insead of using MLE to direcly train the model, we use cross entropy in the training process. This means that we treat the prediction as a classification task. At the inference time, we still use the expectation of labels as the prediction, as this would minimize the RMSE.

```
from our_methods import impute_by_DiscreteLabelClassification

reg = 0.0006625
dim_embed = 8
prediction, loss_monitor, param_list = impute_by_DiscreteLabelClassification(train_matrix, is_train, dim_embed=dim_embed, iters=2000, reg=reg)
train_rmse, test_rmse = eval_prediction(prediction, train_matrix, is_train), eval_prediction(prediction, test_matrix, is_test)
print(f"Reg: {reg}, Train RMSE: {train_rmse:.4f}, Test RMSE: {test_rmse:.4f}")
```

### Discrete Label Combined

Author: AlgebraLoveme

Time Estimate: ~2min40s for 3000 iterations

Leadboard Loss: 0.97630

Idea: use the same discrete label model, but combine the cross entropy and MSE to train.

```
from our_methods import impute_by_DiscreteLabelCombined

reg = 0.00075
dim_embed = 5
prior_reg = 0
alpha = 0.5

prediction, loss_monitor, param_list = impute_by_DiscreteLabelCombined(train_matrix, is_train, dim_embed=dim_embed, iters=3000, reg=reg, prior_reg=prior_reg, alpha=alpha)
train_rmse, test_rmse = eval_prediction(prediction, train_matrix, is_train), eval_prediction(prediction, test_matrix, is_test)
print(f"Train RMSE: {train_rmse:.4f}, Test RMSE: {test_rmse:.4f}")

```

## Ensembles

Test Code: ./test_codes/test_ensemble.ipynb

Weighted & equally average as described. This serves as a code example.

The test data (with a test_ratio=0.01) is used to estimate the weights of individual predictions.

### SVD plus plus + SVP + ALS + nuclear relaxation

LeadBoard Loss: 0.97640 (weighted), 0.97659 (equal)

### previous + DMF

LeadBoard Loss: 0.97536 (weighted)


## Hyperparameter Tuning

### Binary Search with Automatic Overfitting detection

Author: AlgebraLoveme

Time Estimate: linear on the cost of test function, but log on the precision and hyperparameter bounds.

This function applies a binary search to locate the best possible hyperparameter that does not overfit the data. The hyperparameter should be a continuous value.

The first two arguments of the search function is the lower bound and the upper bound of the hyperparameter. The third argument is a test function which only takes the hyperparameter to be tuned as its only argument, and returns a tuple of (train_rmse, test_rmse). The ```hyper_tol``` argument controls the precision of the searching, and the search stops if the difference in the upper bound and the lower bound is smaller than the tolerance. The ```ovefit_tol``` argument controls the criteria for detection overfitting. If the train_rmse is lower than test_rmse minus the tolerance, then this is regarded as overfitting. The ```underfit_direction``` argument specifies in which way can the hyperparameter change to prevent overfitting. By default, ```underfit_direction=ub```, meaning increasing the hyperparameter prevents overfitting, i.e., this hyperparameter is a kind of regularization. If decreasing the hyperparameter leads to better generalization, then the user should set ```underfit_direction=lb```.

```
from util import binary_search_hyperparam

# we use DLC as an example

# 1. wrap the function with single dependence on the hyperparameter to be tuned. This function must return a tuple of (train_rmse, test_rmse).
def test_func(reg):
    dim_embed = 8
    prediction, loss_monitor, param_list = impute_by_DiscreteLabelClassification(train_matrix, is_train, dim_embed=dim_embed, iters=2000, reg=reg)
    train_rmse, test_rmse = eval_prediction(prediction, train_matrix, is_train), eval_prediction(prediction, test_matrix, is_test)
    print(f"Reg: {reg}, Train RMSE: {train_rmse:.4f}, Test RMSE: {test_rmse:.4f}")
    return train_rmse, test_rmse

# 2. specify the lower bound and upper bound of the hyperparameter to be tuned, and the tolerance for hyperparameter precision and overfitting criteria.
binary_search_hyperparam(0.0006625, 0.00068, test_func, hyper_tol=1e-5, overfit_tol=0.03, underfit_direction="ub")

```

## Performance Evaluation

The evaluation function is ```eval_prediction(prediction, test_matrix, is_test, eval_metric='rmse')``` in ```util.py```. For any development of other metrics, please incorporate it into this wrapper function with a corresponding ```eval_metric``` flag. 

### Evaluate by RMSE

Author: AlgebraLoveme

Time Estimate: ~0.1 sec

The RMSE score is computed by 

$$RMSE = \sqrt{\frac{\sum_{(i,j)\in \Omega} (X^{pred}_{i,j} - X^{test}_{i,j})^2}{\sum_{(i,j)\in \Omega} 1}}$$

```
from util import eval_prediction
from baselines import impute_by_mean

prediction = impute_by_mean(train_matrix, is_train, mean_type='col')

eval_prediction(prediction, train_matrix, is_train, eval_metric='rmse')
# The result is 0.0
eval_prediction(prediction, test_matrix, is_test, eval_metric='rmse')
# The result is 1.0292941184259519
```

