# Computational Intelligence Lab

## Collaborative Filtering Project 2022

### Repo Structure

* The **test_codes** directory contains all the Jupyter scripts we use to test all the individual methods in our baseline and our final ensemble method.
* We develop each method with the unified function interface for easier ensemble development. The specific implementations of each method can be found in following corresponding files:
  * **baselines.py**: Impute by Mean, SVD, ALS, SVP, Nuclear Relaxation, SVD++
  * **bayesian_methods.py**: Bayesian SVD++ (rely on myfm package)
  * **our_methods.py**: DLE, DLC, DLCombined
  * **neural_methods.py**: DMF
* **ensemble.py** is our ensemble framework. You can change the ensemble model config in test_codes/test_ensemble.ipynb conveniently.

For more specific instructions including training and evaluation of each model, see [our development doc](doc.md).

### Usage

1. Data:
   
    Please prepare the data file as following structure:
    ```
    .
    ├──CIL Groupwork # our code base
    ├──data
    |   ├──data_train.csv
    |   ├──sampleSubmission.csv
    ```

2. Environments: 
   
    ```
    python3 -m venv env
    source env/bin/activate
    pip install -r requirements.txt
    ```

3. Train and Test
   
   Now you can run each methods in the Jupyter interactive manner under the directory **test_codes**. It will automatically finish the training and report the evaluation scores one the splited eval set.
   
   Note that for test_ensemble.ipynb, you can freely control what method and its parameters are used in the ensemble and how to train the ensemble framework. Better to look at our source code.

   Our final submission is in ./test_codes/test_ensemble.ipynb.

   Have fun with it!
