import pandas as pd
import numpy as np
import random
from sklearn.model_selection import train_test_split

def _read_df_in_format(root):
    def reformat_id(id):
        # split and reformat the df
        row, col = id.split('_')
        return int(row[1:]), int(col[1:])

    df = pd.read_csv(root)
    df['row'], df['col'] = zip(*df['Id'].map(reformat_id))
    df.drop('Id', axis=1, inplace=True)

    return df

def _infer_shape_from_df(df, n_row, n_col):
    max_row_num, max_col_num = np.max(df['row']), np.max(df['col'])
    if n_row is None and n_col is None:
        # infer the shape
        n_row, n_col = max_row_num, max_col_num
    elif n_row is not None and n_col is not None:
        assert n_row >= max_row_num and n_col >= max_col_num, "The specified n_col and n_row should be larger than the max row_num and max col_num, respectively."
    else:
        raise ValueError("n_row and n_col should be both specified or both infered to avoid ambiguity.")

    return n_row, n_col

def _convert_df_to_matrix(df, n_row, n_col):
    row_id = df['row'].to_numpy() - 1
    col_id = df['col'].to_numpy() - 1
    data_matrix = np.zeros((n_row, n_col), dtype=np.int8)
    data_matrix[row_id, col_id] = df['Prediction'].to_numpy()

    is_provided = data_matrix!=0
    return data_matrix, is_provided


def read_data_in_dense_matrix(root, n_row=None, n_col=None, test_size=None, ensemble_test_size=None, random_state=0):

    df = _read_df_in_format(root)

    n_row, n_col = _infer_shape_from_df(df, n_row, n_col)

    if test_size is not None:
        assert 0 < test_size < 1, "The split ratio of test set should be in the range of (0,1)."
        train_df, test_df = train_test_split(df, test_size=test_size, random_state=random_state)
        train_matrix, is_train = _convert_df_to_matrix(train_df, n_row, n_col)
        if ensemble_test_size is not None:
            # Further split the test set, for training and testing the ensemble regression model.
            ensemble_train_df, ensemble_test_df = train_test_split(test_df, test_size=ensemble_test_size, random_state=random_state)
            ensemble_train_matrix, is_ensemble_train = _convert_df_to_matrix(ensemble_train_df, n_row, n_col)
            ensemble_test_matrix, is_ensemble_test = _convert_df_to_matrix(ensemble_test_df, n_row, n_col)
            return (train_matrix, ensemble_train_matrix, ensemble_test_matrix), (is_train, is_ensemble_train, is_ensemble_test)
        else:
            test_matrix, is_test = _convert_df_to_matrix(test_df, n_row, n_col)
            return (train_matrix, test_matrix), (is_train, is_test)

    else:
        data_matrix, is_provided = _convert_df_to_matrix(df, n_row, n_col)
        return data_matrix, is_provided

def store_dense_matrix_to_submission(sub_sample_path, store_path, data_matrix, clip_min=1, clip_max=5):
    # print("Loading requests specified by submission samples...")
    df = _read_df_in_format(sub_sample_path)
    nrows = df.shape[0]
    # print(f"Storing {nrows} records for submission as requested...")

    row_id = df['row'].to_numpy() - 1
    col_id = df['col'].to_numpy() - 1
    data_matrix = np.clip(data_matrix, clip_min, clip_max)
    df['Prediction'] = data_matrix[row_id, col_id]
    
    def reformat_id(record):
        return f"r{record['row']:.0f}_c{record['col']:.0f}"

    df['Id'] = df.apply(reformat_id, axis=1)
    df = df.drop(['row', 'col'], axis=1)
    df.to_csv(store_path, columns=['Id', 'Prediction'], index=False)

def binary_search_hyperparam(hyper_lb:float, hyper_ub:float, func, hyper_tol:float, overfit_tol:float=0.2, underfit_direction:str="ub"):
    '''
    This function would search for the best hyperparameter via binary search, until hyper_ub - hyper_lb < hyper_tol. The searching criteria is based on the best test rmse.

    This function also considers overfitting behavior. When the train rmse is smaller than test rmse minus the overfit tolerence, then it will consider this as overfit, and try to increase the hyperparam if underfit_direction = "ub" (decrease the hyperparam if direction="lb"). If you want to disable detection for overfitting, just set the overfit tolerance to an extremely large value like 1e10.

    The function should return (train_rmse, test_rmse), and should take only the hyperparam for input.
    Therefore, the user should properly wrap their code as a test function
    '''
    assert hyper_lb <= hyper_ub, "The lower bound should be smaller than the upper bound."
    assert hyper_tol > 0, "Must specify a precision > 0."
    assert overfit_tol > 0, "Must specify a overfit tolerance > 0."
    assert underfit_direction in ["lb", "ub"]

    lb, ub = hyper_lb, hyper_ub
    middle = (lb + ub) / 2

    print("Testing for the lb and ub...")
    ub_train, ub_test = func(ub)
    print("ub stat:", ub_train, ub_test)
    if underfit_direction == "ub":
        assert ub_train + overfit_tol >= ub_test, "Detected that the upper bound overfits when the specified underfitting direction is ub. Please increase ub or increase overfit tolerance."
    lb_train, lb_test = func(lb)
    print("lb stat:", lb_train, lb_test)
    if underfit_direction == "lb":
        assert lb_train + overfit_tol >= lb_test, "Detected that the lower bound overfits when the specified underfitting direction is lb. Please decrease lb or increase overfit tolerance."
    lb_better = (lb_test < ub_test)

    print("Start searching.")
    while (ub - lb) >= hyper_tol:
        mid_train, mid_test = func(middle)
        mid_overfit = (mid_train + overfit_tol < mid_test)
        update_ub = lb_better

        if mid_overfit:
            # If the mid value overfits, then it should be modified towards underfitting direction.
            update_ub = False if underfit_direction == "ub" else True
        elif mid_test >= max(lb_test, ub_test):
            # If the mid value does not overfit, then we cannot decide the search bin when it is worse than the bounds. Terminate the searching and ask the user to specify the new bounds. 
            print("Encountered undecidable middle values. Please specify lb and ub again based on the searching statistics.")
            return lb, ub, lb_test, ub_test

        if update_ub:
            ub = middle
            ub_train, ub_test = mid_train, mid_test
        else:
            lb = middle
            lb_train, lb_test = mid_train, mid_test
        lb_better = (lb_test < ub_test)
        middle = (lb + ub) / 2
        print("Current best input:", lb if lb_better else ub)

    return lb, ub, lb_test, ub_test

def _eval_prediction_by_rmse(prediction, test_matrix, is_test):
    predict_for_test = prediction[is_test]
    gt_for_test = test_matrix[is_test]
    rmse = np.sqrt(np.sum((predict_for_test-gt_for_test)**2) / np.sum(is_test))
    return rmse

def eval_prediction(prediction, test_matrix, is_test, eval_metric='rmse'):
    if eval_metric == 'rmse':
        return _eval_prediction_by_rmse(prediction, test_matrix, is_test)
    else:
        raise ValueError(f"Unsupported eval metric: {eval_metric}")

def clip_prediction(prediction, clip_min=1, clip_max=5):
    return np.clip(prediction, clip_min, clip_max)

def move_param_to_device(param_list, device, scale=True):
    for i in range(len(param_list)):
        if scale:
            param_list[i] = param_list[i] / param_list[i].numel() ** 0.5
        param_list[i] = param_list[i].to(device)
        param_list[i].requires_grad = True

def convert_matrix_to_df(matrix):
    users = []
    movies = []
    predictions = []
    idx_list = np.argwhere(matrix)
    for idx in idx_list:
        i, j = idx[0], idx[1]
        users.append(i)
        movies.append(j)
        predictions.append(matrix[i][j])

    X = pd.DataFrame({'user_id': users, 'movie_id': movies, 'rating': predictions})
    return X

def extract_users_items_predictions(data_pd):
    # both will -1
    users, movies = \
        [np.squeeze(arr) for arr in
         np.split(data_pd.Id.str.extract('r(\d+)_c(\d+)').values.astype(int) - 1, 2, axis=-1)]
    predictions = data_pd.Prediction.values
    # index for users and movies starts at 0
    return users, movies, predictions

def get_data():
    TRAIN_SIZE = 0.9
    train_pd = pd.read_csv(f'../data/data_train.csv')
    train_pd = train_pd.sample(frac=1)
    train_users, train_movies, train_predictions = extract_users_items_predictions(train_pd)
    X = pd.DataFrame({'user_id': train_users, 'movie_id': train_movies, 'rating': train_predictions})
    train_df, test_df = train_test_split(X, train_size=TRAIN_SIZE)
    return train_df, test_df

def get_test(root):
    train_pd = pd.read_csv(root)
    users, movies = \
        [np.squeeze(arr) for arr in
         np.split(train_pd.Id.str.extract('r(\d+)_c(\d+)').values.astype(int) - 1, 2, axis=-1)]
    X = pd.DataFrame({'user_id': users, 'movie_id': movies})
    return X

def bootstrap_rng(num_seed):
    return random.sample(range(1000000), k=num_seed)