import numpy as np
import math

import torch
import torch.nn as nn
import torch.nn.functional as F

class MLP(nn.Module):
    def __init__(self, dim_list:'list[int]'):
        super().__init__()
        self.network = self.init_layers(dim_list)

    def init_layers(self, dim_list):
        layer_list = []
        for i, dim in enumerate(dim_list):
            if i == 0:
                in_dim = dim
            elif i == len(dim_list)-1:
                layer_list.append(nn.Linear(in_dim, dim))
            else:
                layer_list.append(nn.Linear(in_dim, dim))
                layer_list.append(nn.ReLU())
                in_dim = dim
        return nn.Sequential(*layer_list)

    def forward(self, x):
        return self.network(x)

def impute_by_DMF(data_matrix, is_provided, iters:int, dim_embed:int, reg_user:float, reg_item:float, lr:float):
    num_user, num_item = data_matrix.shape
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    print("Using:", device)
    user_encoder = MLP([num_item, num_item//4, num_item//16, num_item//64, dim_embed]).to(device)
    item_encoder = MLP([num_user, num_user//4, num_user//16, num_user//64, dim_embed]).to(device)
    data_matrix = torch.from_numpy(data_matrix).float().to(device)
    is_provided = torch.from_numpy(is_provided).to(device)
    optimizer = torch.optim.Adam([{'params': user_encoder.parameters(), 'lr': lr, 'weight_decay':lr*reg_user},
                {'params': item_encoder.parameters(), 'lr': lr, 'weight_decay':lr*reg_item}])
    
    loss_fn = nn.MSELoss()
    loss_monitor = []
    for i in range(iters):
        optimizer.zero_grad()
        user_embedding = user_encoder(data_matrix)
        item_embedding = item_encoder(data_matrix.T)
        pred = user_embedding @ item_embedding.T # shape (num_user, num_item)
        loss = loss_fn(pred[is_provided], data_matrix[is_provided])
        loss.backward()
        optimizer.step()
        loss_monitor.append(loss.item())

    user_embedding = user_encoder(data_matrix)
    item_embedding = item_encoder(data_matrix.T)
    pred = user_embedding @ item_embedding.T
    return pred.detach().cpu().numpy(), loss_monitor
